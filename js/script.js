
//This array stores each of the steps for coffee crafting
var sections = ['home', 'size', 'caffiene', 'milk-amount', 'milk-type', 'foam', 'flavor', 'whip'];
//Sets position at the beginning of the array.
var i = 0;

//Gives selected radio buttons' labels a class and removes it from unselected ones
$('.opts:checked').parent().addClass('label-checked');

$('.opts').on('change', function() {
	$('.active .opts').parent().removeClass('label-checked');
	$(this).parent().addClass('label-checked');
});

//Gives checkboxes' labels a class when selected and removes when unselected
$('.checks').on('change', function() {
	$(this).parent().toggleClass('label-checked');
});

//Sets variables for later functions
var active = '';
var currentSection = '';

//ON CONTINUE CLICK
$('.continue').on('click', function (){

	if ($('#milkNone').prop('checked') && $('#milk-amount').hasClass('active') ){
		 active = $('.options#' + sections[i+=2]);  // skips Milk Type section
	} else { 
		active = $('.options#' + sections[i+=1]); // make next section current section
	}

	currentSection = $('.options.active');
	currentSection.fadeOut(); //hide active section
	currentSection.removeClass('active');
	active.delay(400).fadeIn(300); // fade in new section
	active.addClass('active'); // bring to front
});


//ON BACK CLICK
$('.back').on('click', function(){
	
	if ($('#milkNone').prop('checked') && $('#foam').hasClass('active') ){
		active = $('.options#' + sections[i-=2]); // skips Milk Type section
	}

	else {
		active = $('.options#' + sections[i-=1]); // make next section current section
	}
	
	currentSection = $('.options.active');
	currentSection.fadeOut(); //hide active section
	currentSection.removeClass('active');
	active.delay(400).fadeIn(300); // fade in new section
	active.addClass('active'); // bring to front
});


//ON Show My Coffee CLICK
$('.pullResult').on('click', function () {
	currentSection = $('.options.active');
	currentSection.fadeOut(); //hide active section
	currentSection.removeClass('active');
	$('#loading').delay(400).fadeIn(300) // fade in new section
	$('#loading').delay(2000).fadeOut(300);
	$('#result').delay(2700).fadeIn(300)
	$('#result').addClass('active'); // bring to front

	craftDrink(); //defined in results.js
});


// Hides or Shows nav in mobile
$('.burger').on('click', function (){
	$("nav").toggle(300);
});

$('.tab').on('click', function (){
 	$('aside').toggleClass('aside-expand');
 });
