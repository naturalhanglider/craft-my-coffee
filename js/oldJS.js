//On click/tap of "Craft My Coffee" (#craft) button...
$('#craft').on('click', function(){
	$('#home').fadeOut(); //hide #home
	$('#size').delay(400).fadeIn(300); //show .size
	$('#home').removeClass('active'); //change #home z-index to default so it doesn't block clicking buttons for .size
	$('#size').addClass('active'); //bring .size to the front so buttons are clickable
	// Save user choice
});

$('#toCaffiene').on('click', function(){
	$('#size').fadeOut(); 
	$('#caffiene').delay(400).fadeIn(300);
	$('#size').removeClass('active'); 
	$('#caffiene').addClass('active');
	// Save user choice
});

$('#toMilkAmount').on('click', function(){
	$('#caffiene').fadeOut(); 
	$('#milk-amount').delay(400).fadeIn(300); 
	$('#caffiene').removeClass('active'); 
	$('#milk-amount').addClass('active');
	// Save user choice
});

$('#toMilkType').on('click', function(){
	$('#milk-amount').fadeOut();
	$('#milk-type').delay(400).fadeIn(300);
	$('#milk-amount').removeClass('active');
	$('#milk-type').addClass('active');
	// Save user choice
});

$('#toFoam').on('click', function(){
	$('#milk-type').fadeOut();
	$('#foam').delay(400).fadeIn(300);
	$('#milk-type').removeClass('active');
	$('#foam').addClass('active');
	// Save user choice
});

$('#toFlavor').on('click', function(){
	$('#foam').fadeOut();
	$('#flavor').delay(400).fadeIn(300);
	$('#foam').removeClass('active');
	$('#flavor').addClass('active');
	// Save user choice
});

$('#toWhip').on('click', function(){
	$('#flavor').fadeOut();
	$('#whip').delay(400).fadeIn(300); 
	$('#flavor').removeClass('active');
	$('#whip').addClass('active');
	// Save user choice
});


//BACK BUTTONS
//How can I make a single function that uses the sections array (line 25?)

//On click/tap of "Craft My Coffee" (#craft) button...
$('#backHome').on('click', function(){
	$('#size').fadeOut();
	$('#home').delay(400).fadeIn(300);
	$('#size').removeClass('active');
	$('#home').addClass('active');
});

$('#backSize').on('click', function(){
	$('#caffiene').fadeOut();
	$('#size').delay(400).fadeIn(300);
	$('#caffiene').removeClass('active');
	$('#size').addClass('active');
});

$('#backCaffiene').on('click', function(){
	$('#milk-amount').fadeOut();
	$('#caffiene').delay(400).fadeIn(300);
	$('#milk-amount').removeClass('active');
	$('#caffiene').addClass('active');
});

$('#backMilkAmount').on('click', function(){
	$('#milk-type').fadeOut();
	$('#milk-amount').delay(400).fadeIn(300);
	$('#milk-type').removeClass('active');
	$('#milk-amount').addClass('active');
});

$('#backMilkType').on('click', function(){
	$('#foam').fadeOut();
	$('#milk-type').delay(400).fadeIn(300);
	$('#foam').removeClass('active');
	$('#milk-type').addClass('active');
});

$('#backFoam').on('click', function(){
	$('#whip').fadeOut();
	$('#foam').delay(400).fadeIn(300);
	$('#whip').removeClass('active');
	$('#foam').addClass('active');
});