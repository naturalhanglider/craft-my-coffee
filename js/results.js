
var plain = {
	name: "coffee",
	coffee: 0,
	milk: 0, 
	espresso: 0,
	foam: 0,
	water: 0
};

var latte = {
	name: "latte",
	coffee: 0,
	milk: 25,
	espresso: 75,
	caffiene: 75,
	foam: 1,
	water: 0
};

var americano = {
	name: "Americano",
	coffee: 0,
	milk: 0,
	espresso: 66,
	caffiene: 66,
	foam: 0,
	water: 33
};

var cappucino = {
	name: "cappucino",
	coffee: 0,
	milk: 33,
	espresso: 33,
	caffiene: 33,
	foam: 2,
	water: 33
};

var breve = {
	name: "breve",
	coffee: 0,
	milk: 49, //must be creamer
	espresso: 49,
	foam: 1,
	caffiene: 49,
	water: 0
};

var flatWhite = {
	name: "flat white",
	coffee: 0,
	milk: 66,
	espresso: 33,
	caffiene: 33,
	foam: 0,
	water: 0,
};

var redEye = {
	name: "red eye",
	coffee: 75,
	milk: 0,
	espresso: 1,
	caffiene: 34,
	foam: 0,
	water: 0,
};

var blackEye = {
	name: "black eye",
	coffee: 66,
	milk: 0,
	espresso: 33,
	caffiene: 44,
	foam: 0,
	water: 0,
};

var deadEye = {
	name: "dead eye",
	coffee: 57,
	milk: 0,
	espresso: 43,
	caffiene: 50,
	foam: 0,
	water: 0,
};

var espresso = {
	name: "espresso",
	coffee: 0,
	milk: 0,
	espresso: 100,
	caffiene: 100,
	foam: 0,
	water: 0,
};

var coffeeTypes = [plain, latte, americano, cappucino, breve, blackEye, redEye, deadEye, espresso];

var craftDrink = function (){
	var userChoices = {};
	$(".opts:checked").each(function() {
		var opt = $(this);
		var value = Number(opt.val());
		if (isNaN(value)) {
			value = opt.val();
		}
		userChoices[opt.attr('name')] = value;
	});

	//.grep goes through an array and removes anything that doesn't match the return statement.
	var matchCaffiene = userChoices.caffiene === 0 ? coffeeTypes : $.grep(coffeeTypes, function (a){
		return userChoices.caffiene >= a.caffiene; //removes coffees w/ caff more than user choice
	});
	//drink cannot exceed user's caffiene amount choice
	var matchMilk = $.grep(matchCaffiene, function (a) {
		return userChoices.milkAmount >= a.milk;
	});
	//filters exact match for foam
	var finalMatch = $.grep(matchMilk, function (a) {
		return userChoices.foam === a.foam;
		console.log(finalMatch);
	});

	//.map takes the return statement and generates an array of those things
	var flavors = $('.flavor:checked').map(function(){
		return $(this).val();
	}).get();//this is needed so only the array happens

	//"Your drink is a medium..."
	var allResults = $.map(finalMatch, function (drink) {

		var currentResult = userChoices.size+' ';

		//"...decaf mocha hazelnut..."
		if (userChoices.caffiene == '0') {
			currentResult += 'decaf ';
		}

		currentResult += flavors.join(' ')+' '+drink.name+' ';

		//"...with soy milk...""
		if (userChoices.milkType !== '2% milk') {
			currentResult += 'with '+userChoices.milkType+' ';
		}
		//"...and whip cream."
		currentResult += userChoices.whip;

		return currentResult;
	});


	//If no matches
	var $results = $('#result .main');
	if (allResults.length === 0) {
		$results.html('Sorry, no matches.');
	}
	//If a single match
	else if (allResults.length === 1) {
		$results.html('<strong>Here is your drink:</strong><br><br>A '+allResults[0]);
	}
	//If multiple matches
	else {
			$results.html('<strong>Here are all your drinks:</strong><br><br>A '+allResults.join('<br><br>A '));
	}

	$results.append('<br><button id="tryagain">Try Again.</button>');


	//On Try again click -- needs to be here because it's after allResults is written
	$('#tryagain').on('click', function () {
		currentSection = $('.options.active');
		currentSection.fadeOut(); //hide active section
		currentSection.removeClass('active');
		active = $('.options#' + sections[0]);
		active.delay(400).fadeIn(300); // fade in new section
		active.addClass('active'); // bring to front
		i=0;
	});
};

$('#surprise').on('click', function () {
	currentSection = $('.options.active');
	currentSection.fadeOut(); //hide active section
	currentSection.removeClass('active');
	$('#loading').delay(400).fadeIn(300) // fade in new section
	$('#loading').delay(2000).fadeOut(300);
	$('#result').delay(2900).fadeIn(300)
	$('#result').addClass('active'); // bring to front

	surpriseMe();
});


//Couldn't figure out how to push each value to an array (see commented out section)
// below. Manually enetered these with some values for the time-being.
var allSizes = ['8 oz.','small','medium','large'];
var allFlavors = ['mocha','marshmallow','vanilla','hazelnut'];

var surpriseMe = function () {
	var $results = $('#result .main');
	var randSize = allSizes[Math.floor(Math.random() * allSizes.length)];
	var randFlavor = allFlavors[Math.floor(Math.random() * allFlavors.length)];
	var randDrink = coffeeTypes[Math.floor(Math.random() * coffeeTypes.length)];

	$results.html('SURPRISE! Here is your drink:<br><br>'+randSize+' '+randFlavor+' '+randDrink.name);

	// $('#main .opts[name="size"]').each(function() {
 //    	allSizes.push(this.val());
 //    })

	// $('#main .opts[name="flavor"]').each(function() {
 //    	allFlavors.push(this.val());
 //    })
	$results.append('<br><button id="tryagain">Try Again.</button>');

	$('#tryagain').on('click', function () {
			currentSection = $('.options.active');
			currentSection.fadeOut(); //hide active section
			currentSection.removeClass('active');
			active = $('.options#' + sections[0]);
			active.delay(400).fadeIn(300); // fade in new section
			active.addClass('active'); // bring to front
			i=0;
	});
};


