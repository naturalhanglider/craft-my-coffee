#Craft My Coffee
###Sept 2015
Working on the following:
* combine all coffee choice screens into one view
* let users easily change options through the process
* have results automatically produced based on options user has selected so far
* export results to file on click

####May 2015
This works enough to present as a final project for a course, but users aren't able to save their results.
And not all results are producing correctly. To be continued...
